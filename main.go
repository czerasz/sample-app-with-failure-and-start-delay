package main

import (
	"bytes"
	"context"
	"errors"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"text/template"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const maxPerc = 100

// parseFailRate parses the fail rate coming from environment variable.
func parseFailRate(raw string) (failRate uint64) {
	if raw != "" {
		var err error
		failRate, err = strconv.ParseUint(raw, 10, 8)

		if err != nil {
			log.Printf("can NOT parse FAIL_RATE: %s", err)
			log.Printf("using default value")

			failRate = 0
		}

		if failRate > maxPerc {
			log.Printf("fail rate must be between 0 and 100")
			log.Printf("using default value")

			failRate = 0
		}
	}

	log.Printf("fail rate set at: %d", failRate)

	return
}

// parseReadyAfter parses the ready after time coming from environment variable.
func parseReadyAfter(raw string) (wait uint64) {
	if raw != "" {
		var err error
		wait, err = strconv.ParseUint(raw, 10, 8)

		if err != nil {
			log.Printf("can NOT parse READY_AFTER: %s", err)
			log.Printf("using default value")
		}

		log.Printf("ready after set at: %d seconds", wait)
	}

	return
}

// data represents the input passed to the template.
type data struct {
	Message  string
	FailRate uint64
	Color    string
}

// render generates HTML from a template.
func render(d data) []byte {
	var r []byte
	b := bytes.NewBuffer(r)

	t, err := template.ParseFiles("index.html.tpl")
	if err != nil {
		return nil
	}

	err = t.Execute(b, d)

	if err != nil {
		log.Fatalf("error while parsing template: %+v", err)
	}

	return b.Bytes()
}

// GracefulServer abstracts graceful shutdowns.
type GracefulServer interface {
	Start()
	Stop()
}

// HTTPServer implements GracefulServer.
type HTTPServer struct {
	name   string
	server *http.Server
}

// Start the server.
func (s HTTPServer) Start() {
	log.Printf("%s server listen on: %s\n", s.name, s.server.Addr)

	if err := s.server.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
		log.Fatalf("%s server error: %+v", s.name, err)
	}
}

// Stop the server.
func (s HTTPServer) Stop() {
	timeout := 5
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)

	defer func() {
		cancel()
	}()

	if err := s.server.Shutdown(ctx); err != nil {
		log.Fatalf("%s server shutdown (%s) failed: %+v", s.name, s.server.Addr, err)
	}

	log.Printf("%s server (%s) exited properly", s.name, s.server.Addr)
}

// NewMainSrv initializes new main logic server.
func NewMainSrv(failRate int64, out []byte) GracefulServer {
	m := http.NewServeMux()
	m.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if rand.Int63n(maxPerc) < failRate {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		_, _ = w.Write(out)
	})

	srv := &http.Server{
		Addr:    ":8080",
		Handler: m,
	}

	return HTTPServer{
		name:   "main",
		server: srv,
	}
}

// NewHealthSrv initializes new health server.
func NewHealthSrv(wait uint64) GracefulServer {
	var healthy bool

	m := http.NewServeMux()
	m.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		if healthy {
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write([]byte("healthy"))

			return
		}
		w.WriteHeader(http.StatusNotFound)
		_, _ = w.Write([]byte("not healthy"))
	})

	_ = time.AfterFunc(time.Second*time.Duration(wait), func() {
		log.Printf("application is ready")
		healthy = true
	})

	srv := &http.Server{
		Addr:    ":8081",
		Handler: m,
	}

	return HTTPServer{
		name:   "health",
		server: srv,
	}
}

// NewMerticSrv initializes new mertic server.
func NewMerticSrv() GracefulServer {
	m := http.NewServeMux()
	m.Handle("/metrics", promhttp.Handler())

	srv := &http.Server{
		Addr:    ":2112",
		Handler: m,
	}

	return HTTPServer{
		name:   "metrics",
		server: srv,
	}
}

func main() {
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	failRate := parseFailRate(os.Getenv("FAIL_RATE"))
	wait := parseReadyAfter(os.Getenv("READY_AFTER"))

	d := data{
		Message:  "Hello World",
		FailRate: failRate,
		// Color:    "red",
		Color: os.Getenv("COLOR"),
	}
	out := render(d)

	mainSrv := NewMainSrv(int64(failRate), out)
	go mainSrv.Start()

	healthSrv := NewHealthSrv(wait)
	go healthSrv.Start()

	merticSrv := NewMerticSrv()
	go merticSrv.Start()

	<-sigchan
	log.Printf("stopped")

	mainSrv.Stop()
	healthSrv.Stop()
	merticSrv.Stop()
}
