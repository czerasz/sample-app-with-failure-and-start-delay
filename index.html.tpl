<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Fail Rate App</title>

  <style>
    html {
      width: 100%;
      height: 100%;
    }

    body {
{{- with .Color }}
      background-color: {{printf "%s" .}};
      color: rgb(15, 15, 15);
{{- else }}
      background: rgb(0,34,87); background: linear-gradient(31deg, rgba(0,34,87,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%);
      color: darkblue;
{{- end }}
    }
  </style>
</head>

<body>
  <span style="text-align: center;">
    <h1>{{ .Message }}</h1>
    <p>This app will fail at {{ .FailRate }}%</p>
  </span>
</body>
</html>