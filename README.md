# Golang Application with Specific Features

Features:

- expose the application on port `8080`

  Use the `FAIL_RATE` (from `0-100` percent) environment variable to specify in how many cases the application should return a response with status `500`.
  Use the `COLOR` environment variable to change the background color.

- expose Prometheus metrics (`/metrics` endpoint) on port `2112`

- expose the `/health` endpoint on port `8081`

  Use the `READY_AFTER` environment variable to specify after how many seconds the health endpoint should return successful (status `200`) responses.