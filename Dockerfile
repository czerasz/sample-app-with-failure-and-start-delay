FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0
# Set the current working directory inside the container
WORKDIR /src
# Copy the go modules dependency requirement files
COPY go.mod go.sum ./
# Download dependecies
RUN go mod download

# Copy all source files
COPY . .
# Build the application
RUN go build -o /out/app .

FROM alpine:3 AS bin
COPY --from=build /out/app /server
COPY ./index.html.tpl /index.html.tpl
EXPOSE 8080 8081 2112
ENTRYPOINT [ "/server" ]
