# 1.0.0 (2020-09-26)


### Bug Fixes

* linting ([ac46dc9](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/ac46dc997160e7b974637dc92afae4f897bdfe13))


### Builds

* add Docker support ([ad722a3](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/ad722a3b62484bd153d909501c1f27f19b0ac863))


### Continuous Integrations

* add CI configuration ([65ae929](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/65ae929301745d14f183d9150b9aab20e0681537))
* add release stage ([6f86448](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/6f864484ba6452166df0df0ee69a817d053e9c96))
* fix YAML linting issue ([fba860b](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/fba860b4893c6cd63857f61707b845caaf0cc2c5))


### Documentation

* add documentation ([c1b5317](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/c1b5317c2fe2c97fc30313f6f170351d7a89e61e))


### Features

* add env vars parsing logic ([67b0662](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/67b0662bd1756b9eacf94622acab7851c2d3d009))
* add graceful shutdown ([1aa073d](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/1aa073d5513d1b433126ef8f0cfb30cafe165683))
* add health endpoint ([8822fc0](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/8822fc00f889f82848335130bc3fd77b82ec8a20))
* add mertics endpoint ([0ecb1c6](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/0ecb1c6cb63d0ddaf3edea5ed636efa5a285dca7))
* add rendering logic ([65d8c18](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/65d8c18209cb9b0c44a0e588112692c7a8c3c2fe))
* add server logic ([2eff01d](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/2eff01dc8381d97d2f8a127b629ea1df92caaed7))


* initialize go project ([01494bd](https://gitlab.com/czerasz/sample-app-with-failure-and-start-delay/commit/01494bd5a2f04318442342c811632a769a614e32))
